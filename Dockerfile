FROM python:3.7.6-buster

ENV CONFIG_DIR=/credentials/

WORKDIR /src
COPY requirements.txt /src/
RUN pip install -r requirements.txt

RUN mkdir -p /credentials/
ADD config_dir/gcloud-service-account-key.json /credentials/

COPY *.py /src/
RUN chmod a+x *.py

CMD ["./app_etl.py"]