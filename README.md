# ETL Pipeline Covid-19

This repository is about an ETL (Extract, Transform and Load) of coronavirus data, referring to the state of Ceará.

## Architecture

![arq](https://user-images.githubusercontent.com/17646546/87234739-6a177400-c3aa-11ea-9033-19ff35d477d6.png)

## Public API

Extract data from a public API called IntegraSUS.

![ce-SUS](https://user-images.githubusercontent.com/17646546/83458857-a49c0180-a439-11ea-9f60-8ca994680a22.png)

### Method GET

> GET https://indicadores.integrasus.saude.ce.gov.br/api/casos-coronavirus

## Quick Usage

### Docker Build

Build an image called **etl-covid**:

```bash
$ docker build -t etl-covid .
```

### Docker Run

Run the image **etl-covid**:

```bash
$ docker run etl-covid
```

## Features

* Extract data from a public API;
* Transform data;
* Load to Google BigQuery;
* Cloud Scheduler (unfinished);
* Graphs on Google Dashboard.
