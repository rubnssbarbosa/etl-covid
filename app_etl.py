#!/usr/local/bin/python

import logging
import sys
import os
import datetime
import requests
import json
import pandas as pd

from gcp_utils import upload_to_bigquery
from config import tables_to_export

logging.basicConfig(stream=sys.stdout,
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")

logger = logging.getLogger(__name__)


def extracting():
    logger.info('EXTRACTING...')
    # extract data from API
    url = 'https://indicadores.integrasus.saude.ce.gov.br/api/casos-coronavirus'
    req = requests.get(url)
    data = req.json()
    df = pd.DataFrame(data)
    return df

def transforming(df):
    logger.info('TRANSFORMING...')
    logger.info('REMOVING COLUMNS')
    list_drop_columns = ['estadoPaciente', 'codigoMunicipioPaciente', 'dataNotificacao', 'bairroPaciente', 'dataColetaExame',
    'dataResultadoExame', 'resultadoFinalExame', 'dataSolicitacaoExame', 'dataObito', 'obitoConfirmado', 'paisPaciente']
    df.drop(list_drop_columns, axis=1, inplace=True)

    logger.info('SORTING THE DATE IN ASCENDING ORDER')
    df_covid = df.sort_values(['dataInicioSintomas'], ascending=[1])

    logger.info('NORMALIZING THE DATE')
    df_covid['dataInicioSintomas'] = pd.to_datetime(df_covid['dataInicioSintomas'], utc=False)
    df_covid['dataInicioSintomas'] = df_covid['dataInicioSintomas'].dt.date
    print(df_covid[:5])

    logger.info('FILTERING ON DATE')
    df_covid = df_covid[df_covid['dataInicioSintomas'] == datetime.date.today()]

    logger.info('CONVERT TYPES OF OBJECT TO NUMERIC AND DATETIME')
    df_covid["idadePaciente"] = pd.to_numeric(df_covid["idadePaciente"])
    df_covid["dataInicioSintomas"]= pd.to_datetime(df_covid["dataInicioSintomas"]) 
    return df_covid

def loading(gcp_config, df_covid):
    for table_to_export in tables_to_export:
        table = table_to_export['table']
        bquery_dataset = table_to_export['bquery_dataset']
        bquery_table_schema = table_to_export['schema_bigquery']
        logger.info('UPLOAD TO BIG QUERY')
        upload_to_bigquery(gcp_config, df_covid, table.lower(), bquery_dataset, bquery_table_schema)

def environment_var_config():
    # directory containing the config file with credentials to GCP
    config_dir = os.getenv('CONFIG_DIR', "value does not exist")
    logger.info("CONFIG DIR... %s", config_dir)
    if not os.path.exists(config_dir):
        logger.error("%s not found", config_dir)
        exit(-1)
    
    gcp_config = os.path.join(config_dir, "gcloud-service-account-key.json")
    return gcp_config


if __name__ == "__main__":

    logger.info('START ETL')
    df = extracting()
    logger.info('EXTRACTED')
    df_covid = transforming(df)
    logger.info('TRANSFORMED')
    gcp_config = environment_var_config()
    loading(gcp_config, df_covid)
    logger.info('ETL COMPLETED.')
