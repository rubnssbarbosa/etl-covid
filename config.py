from google.cloud import bigquery

dataset_location = "southamerica-east1"

SCHEMA_COVID= [
    bigquery.SchemaField(name="codigoPaciente", field_type="STRING", mode="NULLABLE"),
    bigquery.SchemaField(name="municipioPaciente", field_type="STRING", mode="NULLABLE"),
    bigquery.SchemaField(name="sexoPaciente", field_type="STRING", mode="NULLABLE"),
    bigquery.SchemaField(name="idadePaciente", field_type="INTEGER", mode="NULLABLE"),
    bigquery.SchemaField(name="dataInicioSintomas", field_type="DATE", mode="NULLABLE"),
]

tables_to_export = [{
    "table": "covid_ceara",
    "bquery_dataset": "covid_data",
    "schema_bigquery": SCHEMA_COVID
}]

