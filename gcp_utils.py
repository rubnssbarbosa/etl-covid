import logging
import sys

from google.cloud.exceptions import NotFound
from google.cloud import bigquery
from google.oauth2 import service_account
from config import dataset_location

logging.basicConfig(stream=sys.stdout,
                    level=logging.INFO,
                    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")

logger = logging.getLogger(__name__)


def dataset_exists(client, dataset_ref):
    try:
        client.get_dataset(dataset_ref)
        return True
    except NotFound:
        return False

def upload_to_bigquery(key_path, df, table_nm, dataset_nm, SCHEMA):
    try:
        credentials = service_account.Credentials.from_service_account_file(
            key_path,
            scopes=["https://www.googleapis.com/auth/cloud-platform"],
        )

        client = bigquery.Client(
            credentials=credentials,
            project=credentials.project_id,
        )

        dataset = bigquery.Dataset('{}.{}'.format(client.project, dataset_nm))
        dataset.location = dataset_location
        if not dataset_exists(client, dataset):
            dataset = client.create_dataset(dataset)  # Make an API request.
            logger.info("CREATED DATASET {}.{}".format(client.project, dataset.dataset_id))
        
        table_id = '{}.{}'.format(dataset.dataset_id, table_nm)

        job_config = bigquery.LoadJobConfig(schema=SCHEMA)
        job = client.load_table_from_dataframe(
            df, table_id, job_config=job_config
        )
        # Wait for the load job to complete.
        job.result()
        logger.info("UPLOADING %s COMPLETED", table_id)
    except Exception as e:
        logger.exception(e)
        logger.error("UPLOADING %s FAILED!", table_id)
        